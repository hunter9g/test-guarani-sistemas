# test_guarani_sistemas

A new Flutter project.

## Getting Started

## Commands

## generating splash screen

flutter clean

flutter pub get

flutter pub run flutter_native_splash:create

## get packages and run code at first time

flutter pub get

## run

flutter run

## test

flutter test

## build

flutter build

## build apk

flutter build apk

## build release

flutter build apk --release

## Assets

The `assets` directory houses images, fonts, and any other files you want to
include with your application.

The `assets/images` directory contains [resolution-aware
images](https://flutter.dev/docs/development/ui/assets-and-images#resolution-aware).

## Localization

This project generates localized messages based on arb files found in
the `lib/src/localization` directory.

To support additional languages, please visit the tutorial on
[Internationalizing Flutter
apps](https://flutter.dev/docs/development/accessibility-and-localization/internationalization)
