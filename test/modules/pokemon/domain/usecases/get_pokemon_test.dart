import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_pokemon.dart';
import 'package:flutter_test/flutter_test.dart';

class PokemonRepositoryMock implements PokemonRepository {
  @override
  Future<Either<FailGetPokemon, List<Pokemon>>> getPokemons() async {
    await Future.delayed(const Duration(seconds: 2));

    return const Right(<Pokemon>[]);
  }

  Future<void> init() {
    throw UnimplementedError();
  }

  @override
  Future<void> addPokemonToFavorites(Pokemon pokemon) {
    throw UnimplementedError();
  }

  @override
  Future<Either<FailGetPokemon, List<Pokemon>>> getFavorites() {
    throw UnimplementedError();
  }

  @override
  Future<void> initHive() {
    throw UnimplementedError();
  }

  @override
  Future<void> removePokemonFromFavorites(int index) {
    throw UnimplementedError();
  }

  @override
  Future<Either<FailGetPokemon, PokemonDetail>> getDetailFromPokemonUrl(
      String url) {
    throw UnimplementedError();
  }
}

void main() {
  PokemonRepositoryMock pokemonRepositoryMock = PokemonRepositoryMock();
  GetPokemons getPokemonImpl = GetPokemons(pokemonRepositoryMock);

  test('should return a list of pokemons', () async {
    // when(pokemonRepositoryMock.getPokemons())
    // .thenAnswer((_) async => const Right(<Pokemon>[]));
    final result = await getPokemonImpl.call();
    expect(result, const Right(<Pokemon>[]));
    // expect(result | null, isA<List<Pokemon>());
  });

  //   test('should return a exception', () async {
  //   when(pokemonRepositoryMock.getPokemons())
  //       .thenAnswer((_) async => const Right(<Pokemon>[]));
  //   final result = await usecase.call();
  //   expect(result, isA<Right>());
  //   // expect(result | null, isA<List<Pokemon>());
  // });
}
