import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

class Utils {
  late BuildContext context;
  Utils(this.context);

  Future<bool> showAddPokemonToFavorites({required Pokemon pokemon}) async {
    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => WillPopScope(
            child: AlertDialog(
              // <-- SEE HERE
              title: Wrap(crossAxisAlignment: WrapCrossAlignment.center,
                  // alignment: WrapAlignment.center,
                  // runAlignment: WrapAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.grey[200],
                      child: Image.network(
                        imgUrl + '${pokemon.getId()}.png',
                        width: 80.0,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      pokemon.name!,
                      textAlign: TextAlign.center,
                    )
                  ]),
              content: SingleChildScrollView(
                child: ListBody(
                  children: const <Widget>[
                    Text('Are you sure want to favorite this pokemon?'),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('No'),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                TextButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          Theme.of(context).primaryColor)),
                  child: const Text(
                    'Yes',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            ),
            onWillPop: () async {
              Navigator.pop(context, false);
              return false;
            }));
  }

  Future<bool> showRemovePokemonFromFavorites(
      {required Pokemon pokemon}) async {
    return await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => WillPopScope(
            child: AlertDialog(
              // <-- SEE HERE
              title: Wrap(crossAxisAlignment: WrapCrossAlignment.center,
                  // alignment: WrapAlignment.center,
                  // runAlignment: WrapAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.grey[200],
                      child: Image.network(
                        imgUrl + '${pokemon.getId()}.png',
                        width: 80.0,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      pokemon.name!,
                      textAlign: TextAlign.center,
                    )
                  ]),
              content: SingleChildScrollView(
                child: ListBody(
                  children: const <Widget>[
                    Text(
                        'Are you sure want to remove from favorite this pokemon?'),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('No'),
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                ),
                TextButton(
                  child: const Text(
                    'Yes',
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            ),
            onWillPop: () async {
              Navigator.pop(context, false);
              return false;
            }));
  }

  void message({required String text, required Pokemon pokemon}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Theme.of(context).primaryColor,
        content: Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
          CircleAvatar(
            radius: 16,
            backgroundColor: Colors.grey[200],
            child: Image.network(
              imgUrl + '${pokemon.getId()}.png',
              width: 40.0,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Text(
            text,
          )
        ]),
      ),
    );
  }
}
