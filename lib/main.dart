import 'package:flutter/material.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_detail_from_pokemon.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_favorites.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/remove_pokemon_from_favorites.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/event.dart';
import 'package:flutter_application_1/src/bloc/pokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon/event.dart';
import 'package:flutter_application_1/src/bloc/pokemon/state.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/event.dart';
import 'package:flutter_application_1/src/components/list_tile_pokemon.dart';
import 'package:flutter_application_1/src/pages/favorites.dart';
import 'package:flutter_application_1/src/pages/pokemon_detail.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../injection_container.dart' as ic;
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  await ic.init();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pokemons',
      theme: ThemeData(),
      home: BlocProvider(
        create: ((context) =>
            ic.sl<PokemonBloc>()..add(PokemonFetchList(true))),
        child: PokemonsHomePage(key: key),
      ),
    );
  }
}

class PokemonsHomePage extends StatefulWidget {
  const PokemonsHomePage({Key? key}) : super(key: key);

  @override
  State<PokemonsHomePage> createState() => _PokemonsHomePageState();
}

class _PokemonsHomePageState extends State<PokemonsHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final PokemonBloc pokemonBloc = BlocProvider.of<PokemonBloc>(context);

    return BlocBuilder<PokemonBloc, PokemonState>(
        bloc: pokemonBloc,
        builder: ((context, state) {
          switch (state.runtimeType) {
            case PokemonLoadingState:
              return Scaffold(
                appBar: AppBar(),
                body: Center(
                  child: Text(state.props[0].toString()),
                ),
              );
            case PokemonErrorState:
              return Scaffold(
                appBar: AppBar(
                  actions: [
                    ElevatedButton.icon(
                      onPressed: () => Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (_) => BlocProvider.value(
                                  value: FavoritePokemonBloc(
                                      ic.sl<GetFavorites>(),
                                      ic.sl<RemovePokemonFromFavorites>())
                                    ..add(FavoritePokemonFetchList()),
                                  child: FavoritesPage(
                                      pokemonBlocContext: context),
                                )),
                      ),
                      icon: const Icon(
                        Icons.favorite,
                        size: 24.0,
                      ),
                      label: const Text('Favorites'),
                    )
                  ],
                  title: const Text(
                    'Pokemons',
                  ),
                ),
                body: Center(child: Text(state.props[0].toString())),
              );
            case PokemonLoadedState:
              List<Pokemon> pokemonList = state.props as List<Pokemon>;
              return Scaffold(
                appBar: AppBar(
                  actions: [
                    ElevatedButton.icon(
                      onPressed: () => Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (_) => BlocProvider.value(
                                  value: FavoritePokemonBloc(
                                      ic.sl<GetFavorites>(),
                                      ic.sl<RemovePokemonFromFavorites>())
                                    ..add(FavoritePokemonFetchList()),
                                  child: FavoritesPage(
                                      pokemonBlocContext: context),
                                )),
                      ),
                      icon: const Icon(
                        Icons.favorite,
                        size: 24.0,
                      ),
                      label: const Text('Favorites'),
                    )
                  ],
                  title: const Text(
                    'Pokemons',
                  ),
                ),
                body: ListView.separated(
                    itemBuilder: ((context, index) {
                      Pokemon pokemon = pokemonList[index];

                      return ListTilePokemon(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => BlocProvider.value(
                                        value: PokemonDetailBloc(
                                            ic.sl<GetDetailFromPokemon>())
                                          ..add(
                                              PokemonDetailFetchList(pokemon)),
                                        child: const PokemonDetailPage(),
                                      ))),
                          pokemonBloc: pokemonBloc,
                          pokemon: pokemon);
                    }),
                    separatorBuilder: (context, index) {
                      return const Divider();
                    },
                    itemCount: pokemonList.length),
              );
            default:
              return const Text('Not mapped');
          }
        }));
  }
}
