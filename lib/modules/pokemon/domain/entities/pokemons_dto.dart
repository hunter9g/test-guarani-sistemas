import 'package:hive/hive.dart';
part 'pokemons_dto.g.dart';

class PokemonData {
  int? count;

  String? next;
  String? previous;

  List<Pokemon>? results;

  PokemonData({this.count, this.next, this.previous, this.results});

  PokemonData.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    next = json['next'];
    previous = json['previous'];
    if (json['results'] != null) {
      results = <Pokemon>[];
      json['results'].forEach((v) {
        // results!.add(Pokemon.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['count'] = count;
    data['next'] = next;
    data['previous'] = previous;
    if (results != null) {
      // data['results'] = results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 1)
class Pokemon {
  @HiveField(0)
  String? name;
  @HiveField(1)
  String? url;

  Pokemon({this.name, this.url});

  Pokemon.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['url'] = url;
    return data;
  }

  int getId() {
    return int.parse(url!.split('pokemon/').last.replaceAll('/', ''));
  }
}
