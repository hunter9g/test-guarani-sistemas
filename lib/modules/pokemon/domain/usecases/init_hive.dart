import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';

class InitHive {
  final PokemonRepository pokemonRepository;

  InitHive(this.pokemonRepository);

  Future<void> call() async {
    return pokemonRepository.initHive();
  }
}
