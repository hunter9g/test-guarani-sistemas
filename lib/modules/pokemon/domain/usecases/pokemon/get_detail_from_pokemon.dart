import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';

class GetDetailFromPokemon {
  final PokemonRepository pokemonRepository;

  GetDetailFromPokemon(this.pokemonRepository);

  Future<Either<FailGetPokemon, PokemonDetail>> call(String url) async {
    return pokemonRepository.getDetailFromPokemonUrl(url);
  }
}
