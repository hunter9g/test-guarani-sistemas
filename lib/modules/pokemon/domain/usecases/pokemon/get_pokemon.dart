import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:dartz/dartz.dart';

class GetPokemons {
  final PokemonRepository pokemonRepository;

  GetPokemons(this.pokemonRepository);

  Future<Either<FailGetPokemon, List<Pokemon>>> call() async {
    return pokemonRepository.getPokemons();
  }
}
