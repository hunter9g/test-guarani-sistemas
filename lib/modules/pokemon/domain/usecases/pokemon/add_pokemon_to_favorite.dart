import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';

class AddPokemonToFavorites {
  final PokemonRepository pokemonRepository;

  AddPokemonToFavorites(this.pokemonRepository);

  Future<void> call(Pokemon pokemon) async {
    return pokemonRepository.addPokemonToFavorites(pokemon);
  }
}
