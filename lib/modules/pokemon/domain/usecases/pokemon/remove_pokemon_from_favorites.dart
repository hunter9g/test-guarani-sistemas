import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';

class RemovePokemonFromFavorites {
  final PokemonRepository pokemonRepository;

  RemovePokemonFromFavorites(this.pokemonRepository);

  Future<void> call(int index) async {
    return pokemonRepository.removePokemonFromFavorites(index);
  }
}
