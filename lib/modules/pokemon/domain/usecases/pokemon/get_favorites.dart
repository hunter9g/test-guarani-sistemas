import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';

class GetFavorites {
  final PokemonRepository pokemonRepository;

  GetFavorites(this.pokemonRepository);

  Future<Either<FailGetPokemon, List<Pokemon>>> call() async {
    return pokemonRepository.getFavorites();
  }
}
