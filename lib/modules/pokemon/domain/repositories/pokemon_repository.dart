import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';

abstract class PokemonRepository {
  Future<void> initHive();

  Future<Either<FailGetPokemon, List<Pokemon>>> getPokemons();

  Future<Either<FailGetPokemon, List<Pokemon>>> getFavorites();

  Future<void> addPokemonToFavorites(Pokemon pokemon);

  Future<void> removePokemonFromFavorites(int index);

  Future<Either<FailGetPokemon, PokemonDetail>> getDetailFromPokemonUrl(
      String url);
}
