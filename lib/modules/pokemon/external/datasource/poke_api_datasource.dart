import 'package:dio/dio.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/modules/pokemon/data/datasources/pokemon_datasource.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:hive/hive.dart';

class PokeApiDataSource implements PokemonDataSource {
  final Dio dio;
  late Box<Pokemon> _pokemons;
  late Box<Pokemon> _favoritesPokemons;

  PokeApiDataSource(this.dio);

  @override
  Future<void> initHive() async {
    Hive.registerAdapter(PokemonAdapter());
    _pokemons = await Hive.openBox<Pokemon>('pokemons');
    _favoritesPokemons = await Hive.openBox<Pokemon>('favoritesPokemons');
  }

  Future<void> clear() async {
    await _pokemons.clear();
    await _favoritesPokemons.clear();
  }

  @override
  Future<List<Pokemon>> getPokemon() async {
    if (_pokemons.values.isNotEmpty) {
      return _pokemons.values
          .where(
              (Pokemon pokemon) => !_favoritesPokemons.values.contains(pokemon))
          .toList();
    } else {
      final response = await dio.get(apiUrl + 'pokemon?limit=50');

      List<Pokemon> list = (response.data["results"] as List)
          .map((e) => Pokemon.fromJson(e))
          .toList();

      await _pokemons.addAll(list);

      return _pokemons.values
          .where(
              (Pokemon pokemon) => !_favoritesPokemons.values.contains(pokemon))
          .toList();
    }
  }

  @override
  Future<List<Pokemon>> getFavorites() async {
    return _favoritesPokemons.values.toList();
  }

  @override
  Future<void> addPokemonToFavorites(Pokemon pokemon) async {
    _favoritesPokemons.add(pokemon);

    return;
  }

  @override
  Future<void> removePokemonFromFavorites(int index) async {
    _favoritesPokemons.deleteAt(index);

    return;
  }

  @override
  Future<PokemonDetail> getDetailFromPokemonUrl(String url) async {
    final response = await dio.get(url);

    PokemonDetail pokemonDetail = PokemonDetail.fromJson(response.data);

    return pokemonDetail;
  }
}
