import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

abstract class PokemonDataSource {
  Future<void> initHive();

  Future<List<Pokemon>> getPokemon();

  Future<List<Pokemon>> getFavorites();

  Future<void> addPokemonToFavorites(Pokemon pokemon);

  Future<void> removePokemonFromFavorites(int index);

  Future<PokemonDetail> getDetailFromPokemonUrl(String url);
}
