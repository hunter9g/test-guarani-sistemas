import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';
import 'package:flutter_application_1/modules/pokemon/external/datasource/poke_api_datasource.dart';

class PokemonRepositoryImpl implements PokemonRepository {
  final PokeApiDataSource pokeApiDataSource;

  PokemonRepositoryImpl(this.pokeApiDataSource);

  @override
  Future<void> initHive() async {
    await pokeApiDataSource.initHive();

    return;
  }

  @override
  Future<Either<FailGetPokemon, List<Pokemon>>> getPokemons() async {
    try {
      List<Pokemon> listPokemon = await pokeApiDataSource.getPokemon();

      return Right(listPokemon);
    } catch (e) {
      return Left(GetListPokemonDataSourceError());
    }
  }

  @override
  Future<Either<FailGetPokemon, List<Pokemon>>> getFavorites() async {
    try {
      List<Pokemon> listFavorites = await pokeApiDataSource.getFavorites();

      return Right(listFavorites);
    } catch (e) {
      return Left(GetListPokemonDataSourceError());
    }
  }

  @override
  Future<void> addPokemonToFavorites(Pokemon pokemon) async {
    return pokeApiDataSource.addPokemonToFavorites(pokemon);
  }

  @override
  Future<void> removePokemonFromFavorites(int index) {
    return pokeApiDataSource.removePokemonFromFavorites(index);
  }

  @override
  Future<Either<FailGetPokemon, PokemonDetail>> getDetailFromPokemonUrl(
      String url) async {
    try {
      PokemonDetail pokemonDetail =
          await pokeApiDataSource.getDetailFromPokemonUrl(url);

      return Right(pokemonDetail);
    } catch (e) {
      return Left(GetListPokemonDataSourceError());
    }
  }
}
