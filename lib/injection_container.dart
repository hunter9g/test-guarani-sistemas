import 'package:dio/dio.dart';
import 'package:flutter_application_1/modules/pokemon/data/repositories/pokemon_repository_impl.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/repositories/pokemon_repository.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/add_pokemon_to_favorite.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_detail_from_pokemon.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_favorites.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_pokemon.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/init_hive.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/remove_pokemon_from_favorites.dart';
import 'package:flutter_application_1/modules/pokemon/external/datasource/poke_api_datasource.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerFactory(() => PokemonBloc(
        InitHive(sl()),
        GetPokemons(sl()),
        AddPokemonToFavorites(sl()),
      ));

  sl.registerFactory(() => FavoritePokemonBloc(
      GetFavorites(sl()), RemovePokemonFromFavorites(sl())));

  sl.registerFactory(() => PokemonDetailBloc(GetDetailFromPokemon(sl())));

  sl.registerLazySingleton(() => GetPokemons(sl()));

  sl.registerLazySingleton(() => GetFavorites(sl()));

  sl.registerLazySingleton(() => AddPokemonToFavorites(sl()));

  sl.registerLazySingleton(() => RemovePokemonFromFavorites(sl()));

  sl.registerLazySingleton(() => GetDetailFromPokemon(sl()));

  sl.registerLazySingleton(() => InitHive(sl()));

  sl.registerLazySingleton(() => Pokemon());

  sl.registerLazySingleton<PokemonRepository>(
      () => PokemonRepositoryImpl(sl()));

  sl.registerLazySingleton<PokeApiDataSource>(() => PokeApiDataSource(sl()));

  sl.registerLazySingleton(() => Dio());
}
