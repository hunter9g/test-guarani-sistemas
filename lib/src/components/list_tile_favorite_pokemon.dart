import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/helpers/utils.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/event.dart';

class ListTileFavoritePokemon extends StatelessWidget {
  final FavoritePokemonBloc favoritePokemonBloc;
  final Pokemon pokemon;
  final int index;
  const ListTileFavoritePokemon(
      {Key? key,
      required this.favoritePokemonBloc,
      required this.pokemon,
      required this.index})
      : super(key: key);

  void onRemoveFromFavoritesPokemon({required BuildContext context}) async {
    if (await Utils(context).showRemovePokemonFromFavorites(pokemon: pokemon)) {
      favoritePokemonBloc.add(RemoveFromFavoritePokemon(index, pokemon));

      Utils(context).message(
          text: '${pokemon.name} removed from favorites!', pokemon: pokemon);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.network(
        imgUrl + '${pokemon.getId()}.png',
        width: 50.0,
      ),
      onTap: () {},
      title: Text(pokemon.name!),
      trailing: IconButton(
          onPressed: () => onRemoveFromFavoritesPokemon(context: context),
          icon: Icon(
            Icons.remove_circle,
            color: Theme.of(context).errorColor,
          )),
    );
  }
}
