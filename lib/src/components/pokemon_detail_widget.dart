import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/src/components/list_tile_detail_pokemon.dart';

class PokemonDetailWidget extends StatelessWidget {
  final PokemonDetail pokemonDetail;
  const PokemonDetailWidget({Key? key, required this.pokemonDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(children: [
      ListTileDetailPokemon(
          title: const Text('pokemon name'),
          subtitle: Text(pokemonDetail.name!),
          leading: Image.network(
            imgUrl + '${pokemonDetail.id!}.png',
            width: 50.0,
          ),
          trailing: const Icon(Icons.catching_pokemon)),
      ListTile(
        tileColor: Theme.of(context).dividerColor,
        trailing: const Icon(Icons.power),
        title: const Text('Base stats'),
      ),
      Expanded(
          child: ListView(
        children: [
          ListTileDetailPokemon(
              title: const Text('base experience'),
              subtitle: Text(pokemonDetail.baseExperience!.toString()),
              leading: null),
          ListTileDetailPokemon(
              title: const Text('height'),
              subtitle: Text(pokemonDetail.height!.toString()),
              leading: null),
          ListTileDetailPokemon(
              title: const Text('weight'),
              subtitle: Text(pokemonDetail.weight!.toString()),
              leading: null),
          for (var item in pokemonDetail.stats!)
            ListTileDetailPokemon(
                title: Text(item.stat!.name!),
                subtitle: Text(item.baseStat.toString()),
                leading: null)
        ],
      ))
    ]);
  }
}
