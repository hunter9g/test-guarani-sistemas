import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/helpers/utils.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/src/bloc/pokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon/event.dart';

class ListTilePokemon extends StatelessWidget {
  final PokemonBloc pokemonBloc;
  final Pokemon pokemon;
  final void Function()? onTap;
  const ListTilePokemon(
      {Key? key,
      required this.pokemonBloc,
      required this.pokemon,
      required this.onTap})
      : super(key: key);

  favoritePokemon({required BuildContext context}) async {
    if (await Utils(context).showAddPokemonToFavorites(pokemon: pokemon)) {
      pokemonBloc.add(FavoritePokemon(pokemon));

      Utils(context).message(
          text: '${pokemon.name} added to favorites!', pokemon: pokemon);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.network(
        imgUrl + '${pokemon.getId()}.png',
        width: 50.0,
      ),
      onTap: onTap,
      title: Text(pokemon.name!),
      trailing: IconButton(
          onPressed: () => favoritePokemon(context: context),
          icon: const Icon(
            Icons.favorite_border,
          )),
    );
  }
}
