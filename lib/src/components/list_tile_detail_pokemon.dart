import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';

class ListTileDetailPokemon extends StatelessWidget {
  final Widget title;
  final Widget subtitle;
  final Widget? trailing;
  final Widget? leading;

  const ListTileDetailPokemon(
      {Key? key,
      required this.title,
      required this.subtitle,
      this.trailing,
      this.leading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      trailing: trailing,
      leading: leading,
      title: title,
      subtitle: subtitle,
    );
  }
}
