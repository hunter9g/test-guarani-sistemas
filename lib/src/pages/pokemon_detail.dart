import 'package:flutter/material.dart';
import 'package:flutter_application_1/const/consts.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/state.dart';
import 'package:flutter_application_1/src/components/list_tile_detail_pokemon.dart';
import 'package:flutter_application_1/src/components/pokemon_detail_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PokemonDetailPage extends StatelessWidget {
  const PokemonDetailPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final PokemonDetailBloc pokemonDetailBloc =
        BlocProvider.of<PokemonDetailBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pokemon details'),
      ),
      body: BlocBuilder<PokemonDetailBloc, PokemonDetailState>(
          bloc: pokemonDetailBloc,
          builder: ((context, state) {
            switch (state.runtimeType) {
              case PokemonDetailLoadingState:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case PokemonDetailErrorState:
                return Center(
                  child: Text(state.props[0].toString()),
                );
              case PokemonDetailLoadedState:
                PokemonDetail pokemonDetail = state.props[0] as PokemonDetail;

                return PokemonDetailWidget(pokemonDetail: pokemonDetail);
              default:
                return const Text('Not mapped');
            }
          })),
    );
  }
}
