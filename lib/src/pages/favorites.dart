import 'package:flutter/material.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/state.dart';
import 'package:flutter_application_1/src/bloc/pokemon/bloc.dart';
import 'package:flutter_application_1/src/bloc/pokemon/event.dart';
import 'package:flutter_application_1/src/components/list_tile_favorite_pokemon.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoritesPage extends StatelessWidget {
  final BuildContext pokemonBlocContext;
  const FavoritesPage({Key? key, required this.pokemonBlocContext})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final FavoritePokemonBloc favoritePokemonBloc =
        BlocProvider.of<FavoritePokemonBloc>(context);

    final PokemonBloc pokemonBloc =
        BlocProvider.of<PokemonBloc>(pokemonBlocContext);

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            pokemonBloc.add(PokemonFetchList(false));
            Navigator.of(context).pop();
          },
        ),
        title: const Text('Favorites'),
      ),
      body: BlocBuilder<FavoritePokemonBloc, FavoritePokemonState>(
          bloc: favoritePokemonBloc,
          builder: ((context, state) {
            switch (state.runtimeType) {
              case FavoritePokemonLoadingState:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case FavoritePokemonErrorState:
                return Center(
                  child: Text(state.props[0].toString()),
                );
              case FavoritePokemonLoadedState:
                return ListView.separated(
                    itemBuilder: ((context, index) {
                      Pokemon pokemon = state.props[index] as Pokemon;

                      return ListTileFavoritePokemon(
                        favoritePokemonBloc: favoritePokemonBloc,
                        pokemon: pokemon,
                        index: index,
                      );
                    }),
                    separatorBuilder: (context, index) {
                      return const Divider();
                    },
                    itemCount: state.props.length);
              default:
                return const Text('Not mapped');
            }
          })),
    );
  }
}
