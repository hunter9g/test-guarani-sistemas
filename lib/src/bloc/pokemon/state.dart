import 'package:equatable/equatable.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

abstract class PokemonState extends Equatable {
  const PokemonState();
}

class PokemonLoadingState extends PokemonState {
  final String message;

  const PokemonLoadingState({required this.message});
  @override
  List<Object?> get props => [message];
}

class PokemonErrorState extends PokemonState {
  final String message;

  const PokemonErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class PokemonLoadedState extends PokemonState {
  final List<Pokemon?> pokemonList;

  const PokemonLoadedState({required this.pokemonList});

  @override
  List<Object?> get props => pokemonList;
}
