import 'package:equatable/equatable.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

abstract class PokemonEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class PokemonFetchList extends PokemonEvent {
  final bool isFirstLoading;
  PokemonFetchList(this.isFirstLoading);
}

class FavoritePokemon extends PokemonEvent {
  final Pokemon pokemon;
  FavoritePokemon(this.pokemon);

  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [pokemon];
}

class AddMorePokemons extends PokemonEvent {
  final int value;
  AddMorePokemons(this.value);

  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class SetLoadingPokemon extends PokemonEvent {
  final String message;

  SetLoadingPokemon({required this.message});
}

class PokemonFetchListError extends PokemonEvent {}

class PokemonFetchListEmpty extends PokemonEvent {}
