import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/add_pokemon_to_favorite.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_pokemon.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/init_hive.dart';
import 'package:flutter_application_1/src/bloc/pokemon/event.dart';
import 'package:flutter_application_1/src/bloc/pokemon/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PokemonBloc extends Bloc<PokemonEvent, PokemonState> {
  // late Box box;
  // PokemonService pokemonService;
  final GetPokemons getPokemons;
  final AddPokemonToFavorites addPokemonToFavorites;
  final InitHive initHive;

  PokemonBloc(this.initHive, this.getPokemons, this.addPokemonToFavorites)
      : super(const PokemonLoadingState(message: 'Carregando..')) {
    on<SetLoadingPokemon>((event, emit) {
      emit(PokemonLoadingState(message: event.message));
    });
    on<PokemonFetchList>((event, emit) async {
      if (event.isFirstLoading) {
        await initHive.call();
      }
      Either<FailGetPokemon, List<Pokemon>> _getPokemon =
          await getPokemons.call();

      _getPokemon.fold(
          (exception) =>
              emit(const PokemonErrorState(message: 'something went wrong :(')),
          (pokemonList) => emit(PokemonLoadedState(pokemonList: pokemonList)));
    });
    on<FavoritePokemon>(((event, emit) async {
      await addPokemonToFavorites.call(event.pokemon);

      Either<FailGetPokemon, List<Pokemon>> _getPokemon =
          await getPokemons.call();

      _getPokemon.fold(
          (exception) =>
              emit(const PokemonErrorState(message: 'something went wrong :(')),
          (pokemonList) => pokemonList.isEmpty
              ? emit(const PokemonErrorState(message: 'empty list :('))
              : emit(PokemonLoadedState(pokemonList: pokemonList)));
    }));
    // on<AddMorePokemons>(((event, emit) async {
    //   await pokemonService.addMorePokemons(value: event.value);
    //   PokemonState pokemonLoadedState = await pokemonService.getPokemons();
    //   emit(pokemonLoadedState);
    // }));
  }
}
