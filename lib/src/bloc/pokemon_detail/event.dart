import 'package:equatable/equatable.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

abstract class PokemonDetailEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class PokemonDetailFetchList extends PokemonDetailEvent {
  final Pokemon pokemon;
  PokemonDetailFetchList(this.pokemon);

  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [pokemon];
}

// class PokemonDetailLoaded extends PokemonDetailEvent {
//   final String url;
//   PokemonDetailLoaded(this.url);

//   @override
//   // ignore: todo
//   // TODO: implement props
//   List<Object> get props => [];
// }

class PokemonFetchListError extends PokemonDetailEvent {}

class PokemonFetchListEmpty extends PokemonDetailEvent {}
