import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';
import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_detail_from_pokemon.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/event.dart';
import 'package:flutter_application_1/src/bloc/pokemon_detail/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PokemonDetailBloc extends Bloc<PokemonDetailEvent, PokemonDetailState> {
  final GetDetailFromPokemon getDetailFromPokemon;

  PokemonDetailBloc(this.getDetailFromPokemon)
      : super(PokemonDetailLoadingState()) {
    on<PokemonDetailFetchList>((event, emit) async {
      Either<FailGetPokemon, PokemonDetail> _getDetailFromPokemon =
          await getDetailFromPokemon.call(event.pokemon.url!);

      _getDetailFromPokemon.fold(
          (exception) => emit(const PokemonDetailErrorState(
              message: 'something went wrong :(')),
          (pokemonList) =>
              emit(PokemonDetailLoadedState(pokemonDetail: pokemonList)));
    });
    // on<PokemonDetailLoaded>(((event, emit) async {
    //   emit(PokemonDetailLoadedState(pokemonDetail: event.pokemon));
    // }));
  }
}
