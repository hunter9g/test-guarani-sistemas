import 'package:equatable/equatable.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemon_detail_dto.dart';

abstract class PokemonDetailState extends Equatable {
  const PokemonDetailState();
}

class PokemonDetailLoadingState extends PokemonDetailState {
  @override
  List<Object?> get props => [];
}

class PokemonDetailErrorState extends PokemonDetailState {
  final String message;

  const PokemonDetailErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class PokemonDetailLoadedState extends PokemonDetailState {
  final PokemonDetail pokemonDetail;

  const PokemonDetailLoadedState({required this.pokemonDetail});

  @override
  List<Object?> get props => [pokemonDetail];
}
