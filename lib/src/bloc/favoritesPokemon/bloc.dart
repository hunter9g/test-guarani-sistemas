import 'package:flutter_application_1/modules/pokemon/domain/errors/errors.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/get_favorites.dart';
import 'package:flutter_application_1/modules/pokemon/domain/usecases/pokemon/remove_pokemon_from_favorites.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/event.dart';
import 'package:flutter_application_1/src/bloc/favoritesPokemon/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoritePokemonBloc
    extends Bloc<FavoritePokemonEvent, FavoritePokemonState> {
  // late Box box;
  // PokemonService pokemonService;
  final GetFavorites getFavorites;
  final RemovePokemonFromFavorites removePokemonFromFavorites;

  FavoritePokemonBloc(this.getFavorites, this.removePokemonFromFavorites)
      : super(FavoritePokemonLoadingState()) {
    on<FavoritePokemonFetchList>((event, emit) async {
      Either<FailGetPokemon, List<Pokemon>> favoritePokemonLoadedState =
          await getFavorites.call();
      favoritePokemonLoadedState.fold(
          (exception) => emit(const FavoritePokemonErrorState(
              message: 'something went wrong :(')),
          (pokemonList) => pokemonList.isEmpty
              ? emit(const FavoritePokemonErrorState(
                  message: 'empty list, select some favorites pokemons :)'))
              : emit(FavoritePokemonLoadedState(
                  favoritePokemonList: pokemonList)));
    });
    on<RemoveFromFavoritePokemon>((event, emit) async {
      await removePokemonFromFavorites.call(event.index);
      Either<FailGetPokemon, List<Pokemon>> favoritePokemonLoadedState =
          await getFavorites.call();
      favoritePokemonLoadedState.fold(
          (exception) => emit(const FavoritePokemonErrorState(
              message: 'something went wrong :(')),
          (pokemonList) => pokemonList.isEmpty
              ? emit(const FavoritePokemonErrorState(
                  message: 'empty list, select some favorites pokemons :)'))
              : emit(FavoritePokemonLoadedState(
                  favoritePokemonList: pokemonList)));
    });
  }
}
