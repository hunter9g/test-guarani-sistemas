import 'package:equatable/equatable.dart';

abstract class FavoritePokemonState extends Equatable {
  const FavoritePokemonState();
}

class FavoritePokemonLoadingState extends FavoritePokemonState {
  @override
  List<Object?> get props => [];
}

class FavoritePokemonErrorState extends FavoritePokemonState {
  final String message;

  const FavoritePokemonErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}

class FavoritePokemonLoadedState extends FavoritePokemonState {
  final List<Object?> favoritePokemonList;

  const FavoritePokemonLoadedState({required this.favoritePokemonList});

  @override
  List<Object?> get props => favoritePokemonList;
}
