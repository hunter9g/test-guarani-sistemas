import 'package:equatable/equatable.dart';
import 'package:flutter_application_1/modules/pokemon/domain/entities/pokemons_dto.dart';

abstract class FavoritePokemonEvent extends Equatable {
  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [];
}

class FavoritePokemonFetchList extends FavoritePokemonEvent {}

class RemoveFromFavoritePokemon extends FavoritePokemonEvent {
  final Pokemon pokemon;
  final int index;
  RemoveFromFavoritePokemon(this.index, this.pokemon);

  @override
  // ignore: todo
  // TODO: implement props
  List<Object> get props => [pokemon];
}

class PokemonFetchListError extends FavoritePokemonEvent {}

class PokemonFetchListEmpty extends FavoritePokemonEvent {}
